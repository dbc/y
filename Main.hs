module Main where

import Interpreter
import Data.IORef

main :: IO ()
main = do
    putStrLn "lithp ith lithening"
    putStrLn ""
    buffer <- newIORef ""
    play buffer "> "
    return ()
