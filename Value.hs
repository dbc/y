module Value where

type Primitive = Expression -> Environment -> State

data Value = Nil
           | Boolean Bool
           | Procedure Expression Expression Environment
           | Macro Expression Expression Environment
           | Builtin Primitive
           | Environment Environment

instance Eq Value where
    (==) = equivilentValue

data Expression = Pair Expression Expression
                | Symbol String
                | Value Value 
    deriving (Eq)

instance Show Expression where
    show = serialise

data Environment = Frame Expression Expression Environment
                 | Empty
    deriving (Eq)

type State = (Expression, Environment)

equivilentValue :: Value -> Value -> Bool
equivilentValue Nil Nil = True
equivilentValue _ _ = False

equivilent :: Expression -> Expression -> Bool
equivilent (Pair a b) (Pair c d) =
    equivilent a c && equivilent b d
equivilent (Symbol a) (Symbol b) = a == b
equivilent (Value a) (Value b) = equivilentValue a b
equivilent _ _ = False

serialiseValue :: Value -> String
serialiseValue Nil = "()"
serialiseValue (Boolean False) = "falsity"
serialiseValue (Boolean True) = "truth"
serialiseValue (Procedure a b _) = "<procedure " ++ serialise a ++ " " ++ serialise b ++ ">"
serialiseValue Macro{} = "<macro>"
serialiseValue (Builtin _) = "<procedure>"
serialiseValue (Environment _) = "<environment>"

serialiseListEnd :: Expression -> String
serialiseListEnd (Value Nil) = ")"
serialiseListEnd (Pair car cdr) = " " ++ serialise car ++ serialiseListEnd cdr
serialiseListEnd x = " . " ++ serialise x ++ ")"

serialise :: Expression -> String
serialise (Pair car cdr) =
    "(" ++ serialise car ++ serialiseListEnd cdr
serialise (Symbol name) = name
serialise (Value value) = serialiseValue value

