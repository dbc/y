module Interpreter where

import System.IO
import Read
import Evaluate
import Data.IORef
import Value

lambda :: Expression
lambda = Value $ Builtin x where
    x args env = let (Pair param (Pair body (Value Nil))) = args in
      (Value (Procedure param body env), env)

kappa :: Expression
kappa = Value $ Builtin x where
    x args env = let (Pair param (Pair body (Value Nil))) = args in
      (Value (Macro param body env), env)

topLevel :: Environment
topLevel = Frame (Symbol "falsity") (Value (Boolean False)) 
           (Frame (Symbol "truth") (Value (Boolean True))
           (Frame (Symbol "lambda") lambda
           (Frame (Symbol "kappa") kappa
           (Frame (Symbol "nil") (Value Nil) Empty))))

play :: IORef String -> String -> IO ()
play buffer prompt = do
    putStr prompt
    System.IO.hFlush System.IO.stdout
    parsed <- expectInput (bufferedRead buffer) (bufferedPeek buffer)
    case parsed of
      Left expression -> putStrLn $ serialise $ seperate (expression, topLevel)
      Right err -> do
        writeIORef buffer ""
        putStrLn err
    putStrLn ""
    System.IO.hFlush System.IO.stdout
    play buffer prompt
