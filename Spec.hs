module Main where

import Test.HUnit
import Test.Framework
import Test.Framework.Providers.HUnit
import Interpreter
import Evaluate
import Value

nil :: Expression
nil = Value Nil

nilSelfEval :: Assertion
nilSelfEval = assertEqual "() does not self evaluate"
                         nil
                         (seperate (nil, topLevel))

nilNamed :: Assertion
nilNamed = assertEqual "() is not named nil in toplevel"
                       nil
                       (seperate (Symbol "nil", topLevel))

matchAnyLambdaWrapsNil :: Assertion
matchAnyLambdaWrapsNil = assertEqual "match any lambda does not wrap nil"
                  (Pair nil nil)
                  (seperate (e, topLevel))
    where e = Pair p (Pair nil nil)
          p = Pair (Symbol "lambda")
                    (Pair (Symbol "x")
                          (Pair (Symbol "x") nil))

matchWrapLambdaExtractsNil :: Assertion
matchWrapLambdaExtractsNil = assertEqual
    "match wrap lambda does not extract nil"
    nil
    (seperate (e, topLevel))
  where e = Pair p (Pair nil nil)
        p = Pair (Symbol "lambda")
                 (Pair (Pair (Symbol "x") nil)
                       (Pair (Symbol "x") nil))

canQuoteSymbol :: Assertion
canQuoteSymbol = assertEqual
    "cannot quote symbol"
    (Symbol "turtle")
    (seperate (e, topLevel))
  where e = Pair p (Pair a nil)
        a = Symbol "turtle"
        p = Pair (Symbol "kappa")
                 (Pair (Pair (Symbol "x") nil)
                       (Pair (Symbol "x") nil))

canListArgs :: Assertion
canListArgs = assertEqual
    "cannot list args"
    aa
    (seperate (e, topLevel))
  where e = Pair p (Pair aa nil)
        p = Pair (Symbol "kappa")
                 (Pair (Pair (Symbol "x") nil)
                       (Pair (Symbol "x") nil))
        aa = Pair (Symbol "a") (Pair (Symbol "b") nil)


main :: IO ()
main = defaultMainWithOpts
       [testCase "nil self eval" nilSelfEval,
        testCase "nil named" nilNamed,
        testCase "match any lambda wraps nil" matchAnyLambdaWrapsNil,
        testCase "match wrap lambda extracts nil" matchWrapLambdaExtractsNil,
        testCase "can quote symbol" canQuoteSymbol,
        testCase "can list args" canListArgs]
       mempty
