module Evaluate where

import Value

resolve :: Expression -> Environment -> Expression
resolve e Empty = error ("unbound " ++ (serialise e))
resolve expression (Frame name value parent)
    | equivilent expression name = value
    | otherwise = resolve expression parent

unify :: Expression -> Expression -> Environment -> Environment
unify (Symbol name) x base = Frame (Symbol name) x base
unify (Value a) (Value b) base
    | equivilentValue a b = base
    | otherwise = error "no match"
unify (Pair a b) (Pair c d) base = unify b d $ unify a c base
unify _ _  _ = error "no match"

contract :: Expression -> Environment -> Expression
contract (Pair car cdr) dynamic =  
    let current = seperate (car, dynamic) in
        Pair current $ contract cdr dynamic
contract x dynamic = seperate (x, dynamic) 

expand :: Expression -> Expression -> Environment -> Expression -> State
-- eval body in extended env
expand pattern body static argument =
    let expanded = unify pattern argument static in
        interpret (body, expanded)

combine :: Expression -> Expression -> Environment -> State
-- work out whether the arg needs evaluating
combine (Value (Procedure pattern body static)) ingredient dynamic =
    let argument = contract ingredient dynamic in 
       expand pattern body static argument 
combine (Value (Macro pattern body static)) ingredient _ =
    expand pattern body static ingredient
combine (Value (Builtin x)) ingredient dynamic = x ingredient dynamic
combine a b  _ = error ("no combiner " ++ (serialise a) ++ " " ++ (serialise b))

extract :: Expression -> Expression -> Environment -> State 
-- eval the car of application form
extract expression ingredient environment =
    let combiner = seperate (expression, environment) in
       combine combiner ingredient environment

step :: State -> State
step (Value value, environment) = (Value value, environment)
step (Symbol symbol, environment) = (resolve (Symbol symbol) environment, environment)
step (Pair car cdr, environment) = extract car cdr environment 

isNormalised :: State -> Bool
isNormalised (Value _, _) = True
isNormalised _ = False

interpret :: State -> State
interpret (expression, environment)
    | isNormalised (expression, environment) = (expression, environment)
    -- | otherwise = interpret $ step (expression, environment) -- small step
    | otherwise = step (expression, environment)

seperate :: State -> Expression
seperate = fst . interpret
