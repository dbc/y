module Read where

import qualified System.Console.Readline
import Value
import Data.IORef
import Data.Char

-- TODO addHistoryLine

bufferedRead :: IORef String -> IO Char
bufferedRead state = do
    buffer <- readIORef state
    if null buffer then
        do
            maybeLine <- System.Console.Readline.readline ""
            case maybeLine of
                Nothing -> undefined
                Just "" -> return '\n' 
                Just line ->
                  do writeIORef state (tail line ++ "\n")
                     return $ head line
    else
        let char : xs = buffer in
          do
            writeIORef state xs
            return char

bufferedPeek :: IORef String -> IO Char
bufferedPeek state = do
    buffer <- readIORef state
    if null buffer then
        do
            maybeLine <- System.Console.Readline.readline ""
            case maybeLine of
                Nothing -> undefined
                Just "" -> return '\n' 
                Just line ->
                  do writeIORef state (line ++ "\n")
                     return $ head line
    else
      return $ head buffer


expectInput :: IO Char -> IO Char -> IO (Either Expression String)
expectInput pull peek = do
    char <- pull
    if isSpace char then
      expectInput pull peek
    else
      if isAlpha char then
        expectSymbol pull peek [char] 
      else
        if char == '(' then
          expectList pull peek
        else
          return $ Right ("expectInput -- unexpected " ++ [char])

expectSymbol :: IO Char -> IO Char -> String -> IO (Either Expression String)
expectSymbol pull peek accumulator = do
    char <- peek
    if isSpace char || char == ')' then
        return $ Left (Symbol accumulator)
    else
      if isAlpha char then do
        _ <- pull
        expectSymbol pull peek (accumulator ++ [char])
      else
        return $ Right ("expectSymbol -- unexpected " ++ [char])

expectList :: IO Char -> IO Char -> IO (Either Expression String)
expectList pull peek = do
    char <- peek
    if char == ')' then do
      _ <- pull
      return $ Left (Value Nil)
    else
      if isSpace char then do
        _ <- pull
        expectList pull peek
      else  
        expectElement pull peek

expectElement :: IO Char -> IO Char -> IO (Either Expression String)
expectElement pull peek = do
    maybeElement <- expectInput pull peek
    case maybeElement of
      Right err -> return $ Right ("expectElement car -- " ++ err)
      Left element -> do
        maybeTail <- expectBreak pull peek
        return $ case maybeTail of
          Right err -> Right ("expectElement cdr-- " ++ err) 
          Left continue -> Left (Pair element continue)

expectBreak :: IO Char -> IO Char -> IO (Either Expression String)
expectBreak pull peek = do
    char <- peek
    if char == ')' then do
      _ <- pull
      return $ Left (Value Nil)
    else
      if isSpace char then do
        _ <- pull
        expectContinue pull peek
      else
        return $ Right ("expectBreak -- unexpected " ++ [char])

expectContinue :: IO Char -> IO Char -> IO (Either Expression String)
expectContinue pull peek = do
    char <- peek
    if char == ')' then do
      _ <- pull
      return $ Left (Value Nil)
    else
      if isSpace char then do
        _ <- pull
        expectContinue pull peek
      else
        if char == '.' then do
          _ <- pull
          expectDotted pull peek
        else
          expectElement pull peek

expectDotted :: IO Char -> IO Char -> IO (Either Expression String)
expectDotted pull peek = do
    char <- pull
    if isSpace char then do
      maybeElement <- expectInput pull peek
      case maybeElement of
        Right err -> return $ Right ("expectDotted -- " ++ err)
        Left element -> do
          maybeEnd <- expectEnd pull
          case maybeEnd of
            Just err -> return $ Right ("expectDotted -- " ++ err)
            Nothing -> return $ Left element
    else
      return $ Right ("expectDotted -- unexpected " ++ [char])

expectEnd :: IO Char -> IO (Maybe String)
expectEnd pull = do
    char <- pull
    if isSpace char then
      expectEnd pull
    else
      if char == ')' then
        return Nothing
      else
        return $ Just $ "expectEnd -- unexpected " ++ [char]
